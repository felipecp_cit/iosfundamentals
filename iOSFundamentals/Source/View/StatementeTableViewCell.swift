//
//  StatementeTableViewCell.swift
//  iOSFundamentals
//
//  Created by Felipe Carvalho Pignolatti on 20/07/18.
//  Copyright © 2018 Itaú. All rights reserved.
//

import UIKit

class StatementeTableViewCell: UITableViewCell {
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    /**
    *   Initialize cell
    *
    *   @param value        Statemente value
    *   @param description  Statemente description
    *   @param date         Statemente date
    */
    public func setupCell(value: String, description: String, date: String) {
        self.valueLabel.text = value
        self.descriptionLabel.text = description
        self.dateLabel.text = date
        
        self.applyBorder()
    }
    
    // MARK: Private methods
    private func applyBorder() {
        let color = UIColor(red:151/255, green:151/255, blue:151/255, alpha: 1).cgColor
        let borderWidth:CGFloat = 1.0
        
        let borderLeft = CALayer()
        borderLeft.borderWidth = borderWidth
        borderLeft.borderColor = color
        borderLeft.frame = CGRect(x: 0, y: 0, width: borderWidth, height: self.frame.size.height)
        
        let borderBottom = CALayer()
        borderBottom.borderWidth = borderWidth
        borderBottom.borderColor = color
        borderBottom.frame = CGRect(x: 0, y: self.frame.size.height, width:self.frame.size.width, height: borderWidth)
        
        self.layer.addSublayer(borderLeft)
        self.layer.addSublayer(borderBottom)
        
        let bottomView = UIView.init(frame: CGRect.init(x: 0, y: self.frame.size.height - 2, width: self.frame.size.width, height: 1))
        bottomView.backgroundColor = UIColor.lightGray
        self.addSubview(bottomView)
    }
}
