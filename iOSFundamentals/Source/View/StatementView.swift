//
//  Statement.swift
//  iOSFundamentals
//
//  Created by Felipe Carvalho Pignolatti on 19/07/18.
//  Copyright © 2018 Itaú. All rights reserved.
//

import UIKit

class StatementView: UIView {
    
    // MARK: Public methods
    public func setupView() {
        self.applyBorder()
    }
    
    // MARK: Private methods
    func applyBorder() {
        let color = UIColor(red:151/255, green:151/255, blue:151/255, alpha: 1).cgColor
        let borderWidth:CGFloat = 1.0
        
        let borderLeft = CALayer()
        borderLeft.borderWidth = borderWidth
        borderLeft.borderColor = color
        borderLeft.frame = CGRect(x: 0, y: 0, width: borderWidth, height: self.frame.size.height)
        
        let borderRight = CALayer()
        borderRight.borderWidth = borderWidth
        borderRight.borderColor = color
        borderRight.frame = CGRect(x: 0, y: self.frame.size.height, width:self.frame.size.width, height: borderWidth)
        
        self.layer.addSublayer(borderLeft)
        self.layer.addSublayer(borderRight)
    }
}
