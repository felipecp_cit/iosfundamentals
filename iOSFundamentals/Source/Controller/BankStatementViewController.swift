//
//  ViewController.swift
//  iOSFundamentals
//
//  Created by Felipe Carvalho Pignolatti on 19/07/18.
//  Copyright © 2018 Itaú. All rights reserved.
//

import UIKit

class BankStatementViewController: UIViewController {
    @IBOutlet weak var firstStatement: StatementView!
    @IBOutlet weak var secondStatement: StatementView!
    @IBOutlet weak var thirdStatement: StatementView!
    @IBOutlet weak var fourthStatement: StatementView!
    
    // MARK: Cycle view methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setup()
    }

    // MARK: Private methods
    private func setup() {
        self.view.layoutIfNeeded()
        self.firstStatement.applyBorder()
        self.secondStatement.applyBorder()
        self.thirdStatement.applyBorder()
        self.fourthStatement.applyBorder()
    }
}

