//
//  ViewController.swift
//  iOSFundamentals
//
//  Created by Felipe Carvalho Pignolatti on 19/07/18.
//  Copyright © 2018 Itaú. All rights reserved.
//

import UIKit

class BankStatementTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    private var statements: [StatementModel]?
    private var lastSelected: Int?
    @IBOutlet weak var statementsTableView: UITableView!
    
    private struct Constants {
        static let NumberOfRows = 1
        static let CellHeight: CGFloat = 64.0
        static let CellSpacing: CGFloat = 10.0
    }
    
    // MARK: Cycle view methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.statements = self.getDatasource()
    }
    
    // MARK: UITableViewDelegate and UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Constants.NumberOfRows;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = statements?.count {
            return count
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentCell = tableView.dequeueReusableCell(withIdentifier: "StatementCellIdentifier", for: indexPath)
        if let statementeCell = currentCell as? StatementeTableViewCell, let collection = self.statements {
            let statement: StatementModel = collection[indexPath.item] as StatementModel
            statementeCell.setupCell(value: statement.value, description: statement.description, date: statement.date)
        }
        
        return currentCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CellHeight + Constants.CellSpacing
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.lastSelected = indexPath.item
        self.performSegue(withIdentifier: "BankStatementToTransactionsSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: TransactionsViewController.self) {
            if let transactionsViewController = segue.destination as? TransactionsViewController,
                let collection = self.statements,
                let index = self.lastSelected {
                transactionsViewController.statement = collection[index]
            }
        }
    }

    // MARK: Private methods
    
    func getDatasource() -> [StatementModel] {
        var statements = [StatementModel]()
        statements.append(StatementModel.init(value: "R$ 39,90", description: "Casa da moqueca", date: "12/07/2018"))
        statements.append(StatementModel.init(value: "R$ 120,90", description: "Outback Steakhouse", date: "13/07/2018"))
        statements.append(StatementModel.init(value: "R$ 20,90", description: "Rações da Tokinha", date: "17/07/2018"))
        statements.append(StatementModel.init(value: "R$ 30,00", description: "Sympla", date: "18/07/2018"))
        statements.append(StatementModel.init(value: "R$ 10,00", description: "Subway", date: "21/07/2018"))
        statements.append(StatementModel.init(value: "R$ 1090,10", description: "Geladeira", date: "23/07/2018"))
        return statements
    }
}

