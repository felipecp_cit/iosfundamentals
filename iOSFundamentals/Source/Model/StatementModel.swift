//
//  StatementModel.swift
//  iOSFundamentals
//
//  Created by Felipe Carvalho Pignolatti on 20/07/18.
//  Copyright © 2018 Itaú. All rights reserved.
//

import UIKit

class StatementModel {
    public var value: String
    public var description: String
    public var date: String
    
    public init (value: String, description: String, date:String) {
        self.value = value
        self.description = description
        self.date = date
    }
}
